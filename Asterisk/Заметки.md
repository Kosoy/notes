# Общие заметки по Asterisk

### Добавить что-то из исходников, например русскую озвучку
`cd /usr/src/asterisk-...` 

`make menuselect` 

Добавляем в **Core Sound Packages**:

    CORE-SOUNDS-RU-WAV
    CORE-SOUNDS-RU-ULAW
    CORE-SOUNDS-RU-ALAW

`make`

`make install`
    
В sip.conf раскомментируем и редактируем строчку:

`language=ru`
`systemctl restart asterisk`
***

### Asterisk CDR Viewer Mod
GUI для прослушивания записей разговоров
***

### Голосовая почта
Создаем ящики `/etc/asterisk/voicemail.conf`
***

### Бэкапим
Конфиги: `/etc/asterisk`

БД:      `/var/lib/asterisk/astdb.sqlite3`
***

### Конференция
   1. Установить dahdi (make menuselect).
        Для этого понадобиться: kernel-devil и kernel-headers (проверить версию ядра, чтоб совпадали в системе и в этих пакетах)
   2. Пересобираем Asterisk.
   3. В файле meetme.conf создаем конференции.
  
