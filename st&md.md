# Настройка Sublime Text для работы с Markdown.

1. Устанавливаем **Sublime Text.**
2. Ставим плагин **Package Control** - менеджер пакетов.
3. Ставим плаги **MarkdownEditing** - улучшенный редактор.
4. Ставим плагин **OmniMarkupPreviewer** - live просмотр в браузере, но есть скроллбар внизу. `Ctrl+Alt+O`
5. Ставим плагин **Markdown Preview** - live просмотр, только после сохранения `Ctrl+S` .
